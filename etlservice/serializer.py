from rest_framework import serializers
from .models import EtlService

class EtlServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = EtlService
        fields = '__all__'
