from __future__ import unicode_literals

from django.db import models

class EtlService(models.Model):
    file_name = models.CharField(max_length=255)
    file_location = models.CharField(max_length=255)
    utility = models.CharField(max_length=255)
    utility_format = models.CharField(max_length=10)
    source_file_location = models.CharField(max_length=255)