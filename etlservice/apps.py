from __future__ import unicode_literals

from django.apps import AppConfig


class EtlserviceConfig(AppConfig):
    name = 'etlservice'
