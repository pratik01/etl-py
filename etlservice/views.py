from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import EtlService
from .serializer import EtlServiceSerializer
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
import json
import pickle
from etl_lib.extract.pgne import Pgne
from etl_lib.extract.sce import Sce
from etl_lib.extract.sdgne import Sdgne
class JSONResponse(HttpResponse):
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

class EtlServiceApi(APIView):
    def get(self,request,format=None):
        etlservices = EtlService.objects.all()
        serializer = EtlServiceSerializer(etlservices,many=True)
        return Response(serializer.data)

    @csrf_exempt
    def post(self,request,format=None):
        list_obj = []
        process_list = []
        data = request.data
        #file = request.FILES['file']
        #meterName = request.META['HTTP_METERNAME']
        stage = request.META['HTTP_STAGE']
        extract = request.META['HTTP_EXTRACT']
        extract_format = request.META['HTTP_FORMAT']
        transform = request.META['HTTP_TRANSFORM']
        load = request.META['HTTP_LOAD']
        if(extract=="pge"):
            pgne = Pgne()
            for file in request.FILES.getlist('file'):
                process_list = pgne.process(extract_format, file)
        elif(extract=="sce"):
            sce = Sce()
            for file in request.FILES.getlist('file'):
                process_list = sce.process(extract_format, file)
        elif(extract=="sdge"):
            sdge = Sdgne()
            for file in request.FILES.getlist('file'):
                process_list = sdge.process(extract_format, file)
        for obj in process_list:
            if ('error' in obj):
                data_obj = {}
                data_obj['error'] = obj['error']
                print(obj['error'])
                return JSONResponse(obj['error'], status=200)
            else:
                etlService = EtlService()
                etlService.file_location = obj['file_location']
                etlService.utility = extract
                etlService.utility_format = extract_format
                etlService.file_name = obj["file_name"]
                etlService.source_file_location = obj['source_file_location']
                etlService.save()
                list_obj.append(etlService)
        serializer = EtlServiceSerializer(list_obj, many=True)
        return Response(serializer.data)


class Formats(APIView):
    def get(self,request,format=None):
        data = request.data
        print(data)
        stage = request['stage']
        if(stage=="Extract"):
            str = {
                endpoint:"http://192.168.56.101:8080/extract/",
                utility:"PG&E/SCE/SDG&E",
                utility_format:"Format#1",
                file:"user selected file"
            }
            return JSONResponse(str, status=200)
        elif (stage == "Transform"):
            print("Transform")
        elif (stage == "Load"):
            print("Load")