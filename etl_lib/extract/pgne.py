from common.etl_helper import EtlHelper
from django.conf import settings
import time
import os
class Pgne:
    etl_helper = ""
    recordList = []

    def __init__(self):
        self.etl_helper = EtlHelper()
        self.recordList = []

    def process(self,extract_format,file):
        self.recordList = []
        time_stamp = str(time.time())
        output_location = settings.BASE_DIR + '/upload/PG&E/'+time_stamp+"/"
        if not os.path.exists(output_location):
            os.makedirs(output_location)
        source_file_name_location = output_location +time_stamp+"_"+str(file.name)
        fd = open(source_file_name_location, 'wb')
        for chunk in file.chunks():
            fd.write(chunk)
        fd.close()
        if (extract_format=="Format#1"):
            data_obj = {}
            file_location = source_file_name_location
            usecols = ["DATE", "START TIME", "USAGE"]
            data_obj['file_location'] = file_location
            df = self.etl_helper.read_csv(file_location, ",", 5, usecols)
            df['DATE'] = df.DATE + ' ' + df['START TIME']
            df1 = df[['DATE', 'USAGE']]
            df1['USAGE'] = df1['USAGE'].apply(lambda x: x * 4)
            df1.columns = ['Timestamp', 'Original']
            data_obj['file_name'] = str("peng_loa_output_") + time_stamp + str(".xlsx")
            file_name = str(output_location) + data_obj['file_name']
            self.etl_helper.writer_xls(df1, file_name)
            data_obj['file_location'] = output_location
            data_obj['source_file_location'] = source_file_name_location
            self.recordList.append(data_obj)
        elif (extract_format == "Format#2"):
            xls_file = self.etl_helper.read_xls(source_file_name_location)
            df = xls_file.parse(0, skiprows=13)
            df1 = df[['Period', 'FOSTER CITY   9282698161   901 METRO CENTER BLVD   X45965   E3344 (kWh)']]
            df1.columns = ['Timestamp', 'Original']
            file_name = "PG&E_interact_data_output_1_" + str(time.time()) + ".xlsx"
            self.etl_helper.writer_xls(df1,output_location + file_name)
            data_obj = {}
            data_obj['file_name'] = file_name
            data_obj['file_location'] = output_location
            data_obj['source_file_location'] = source_file_name_location
            self.recordList.append(data_obj)
            df2 = df[['Period', 'FOSTER CITY   5939277005   TOTALIZE    METER 2R0077 2R0109  RN609 (kWh)']]
            df2.columns = ['Timestamp', 'Original']
            file_name = "PG&E_interact_data_output_2_" + str(time.time()) + ".xlsx"
            self.etl_helper.writer_xls(df2, output_location + file_name)
            data_obj = {}
            data_obj['file_name'] = file_name
            data_obj['file_location'] = output_location
            data_obj['source_file_location'] = source_file_name_location
            self.recordList.append(data_obj)
            df3 = df[['Period', 'FOSTER CITY   5897610005   TOTALIZED   METER 5P2526 1003739354 (kWh)']]
            df3.columns = ['Timestamp', 'Original']
            file_name = "PG&E_interact_data_output_3_" + str(time.time()) + ".xlsx"
            self.etl_helper.writer_xls(df3, output_location + file_name)
            data_obj = {}
            data_obj['file_name'] = file_name
            data_obj['file_location'] = output_location
            data_obj['source_file_location'] = source_file_name_location
            self.recordList.append(data_obj)
            print(self.recordList)
        else:
            data_obj = {}
            data_obj['error'] = "Invalid format"
            self.recordList.append(data_obj)
        return self.recordList
