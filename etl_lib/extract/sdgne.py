from common.etl_helper import EtlHelper
from django.conf import settings
import time
import os
class Sdgne:
    etl_helper = ""
    recordList = []

    def __init__(self):
        self.etl_helper = EtlHelper()
        self.recordList = []

    def process(self,extract_format,file):
        self.recordList = []
        time_stamp = str(time.time())
        output_location = settings.BASE_DIR + '/upload/SDG&E/'+time_stamp+"/"
        if not os.path.exists(output_location):
            os.makedirs(output_location)
        source_file_name_location = output_location +time_stamp+"_"+str(file.name)
        fd = open(source_file_name_location, 'wb')
        for chunk in file.chunks():
            fd.write(chunk)
        fd.close()
        if (extract_format=="Format#1"):
            df = self.etl_helper.read_csv(source_file_name_location)
            old_time = "00:00"
            col_len = len(df.columns)
            for index in range(4, col_len):
                col = str(df.columns[index])
                col = col.split("_");
                sub = col[1][:2] + ":" + col[1][2:]
                df.rename(columns={df.columns[index]: old_time}, inplace=True)
                old_time = sub
            my_cols = set(df.columns)
            my_cols.remove('Rate')
            my_cols = list(my_cols)
            df = df[my_cols]
            id_vars = ['Interval Date', 'Meter ID', 'Account Number']
            var_name = ['Start_time']
            res = self.etl_helper.melt(df, id_vars, var_name)
            m_nos = df['Meter ID'].unique()
            a_no = df['Account Number'].unique()
            res = res.sort(['Interval Date', 'Start_time', 'value'], ascending=[1, 1, 1])
            res['Interval Date'] = res['Interval Date'].apply(str) + ' ' + res['Start_time']
            res['value'] = res['value'].apply(lambda x: x * 4)
            for m_no in m_nos:
                data_obj = {}
                data_obj['source_file_location'] = source_file_name_location
                temp_df = res
                df = temp_df[temp_df['Meter ID'] == m_no]
                df = df[['Interval Date', 'value']]
                df.columns = ['Timestamp', 'Original']
                data_obj['file_name'] = str("sdgne_Loa_output_" )+ str(m_no) + "_" + str(time_stamp) + str(".xlsx")
                filename = str(output_location) + data_obj['file_name']
                self.etl_helper.writer_xls(df, filename)
                data_obj['file_location'] = output_location
                self.recordList.append(data_obj)
        elif (extract_format=="Format#2"):
            usecols = ["Meter Number", "Date", "Start Time", "Value"]
            df = self.etl_helper.read_csv(source_file_name_location,',',14,usecols)
            df['Date'] = df['Date'] + ' ' + df['Start Time']
            #df['Date'] = pd.to_datetime(df['Date'], format='%m/%d/%Y %I:%M %p')
            df['Date'] = self.etl_helper.to_datetime(df['Date'],'%m/%d/%Y %I:%M %p')
            df['Date'] = df['Date'].apply(lambda x: x.strftime('%m/%d/%Y %H:%M'))
            df['Value'] = df['Value'].apply(lambda x: x * 4)
            m_nos = df['Meter Number'].unique()
            for m_no in m_nos:
                data_obj = {}
                data_obj['source_file_location'] = source_file_name_location
                temp_df = df
                temp = temp_df[temp_df['Meter Number'] == m_no]
                temp = temp[['Date', 'Value']]
                temp.columns = ['Timestamp', 'Original']
                data_obj['file_name'] = str("sdgne_com_output_") + str(m_no) + "_" + str(time_stamp) + str(".xlsx")
                filename = str(output_location) + data_obj['file_name']
                self.etl_helper.writer_xls(temp, filename)
                data_obj['file_location'] = output_location
                self.recordList.append(data_obj)
        elif (extract_format == "Format#3"):
            df = self.etl_helper.read_csv(source_file_name_location)
            df.rename(columns={df.columns[len(df.columns) - 1]: "24:00"}, inplace=True)
            id_vars = ['Account', 'Date', 'Channel', 'Units']
            var_name = ['Start_time']
            df = self.etl_helper.melt(df, id_vars, var_name)
            df['Date'] = df.Date + ' ' + df['Start_time']
            df = df[['Date', 'value']]
            data_obj = {}
            data_obj['source_file_location'] = source_file_name_location
            data_obj['file_name'] = str("sdgne_kwickview_output_") + "_" + str(time_stamp) + str(".xlsx")
            filename = str(output_location) + data_obj['file_name']
            self.etl_helper.writer_xls(df, filename)
            data_obj['file_location'] = output_location
            self.recordList.append(data_obj)
        else:
            data_obj = {}
            data_obj['error'] = "Invalid format"
            self.recordList.append(data_obj)
        return self.recordList