from common.etl_helper import EtlHelper
from django.conf import settings
import time
import os
class Sce:
    etl_helper = ""
    recordList = []

    def __init__(self):
        self.etl_helper = EtlHelper()
        self.recordList = []

    def process(self,extract_format,file):
        self.recordList = []
        time_stamp = str(time.time())
        output_location = settings.BASE_DIR + '/upload/SCE/'+time_stamp+"/"
        if not os.path.exists(output_location):
            os.makedirs(output_location)
        new_file_name = output_location +time_stamp+"_"+str(file.name)
        fd = open(new_file_name, 'wb')
        for chunk in file.chunks():
            fd.write(chunk)
        fd.close()
        if (extract_format=="Format#1"):
            data_obj = {}
            file_location = new_file_name
            xls_file = self.etl_helper.read_xls(file_location)
            df = xls_file.parse(0, skiprows=2)
            df = df[df['Unit of Measurement'] == 'KW_DEL']
            rm_col_arr = ['Service Account No', 'Unit of Measurement', 'Unnamed: 4']
            df = self.etl_helper.remove_columns_xls(df, rm_col_arr)
            df['Date'] = df['Date'].apply(lambda x: x.date())  # used for remove time from the date field
            id_vars = ['Date', 'Meter Number']
            var_name = ['Start_time']
            res = self.etl_helper.melt(df, id_vars, var_name)
            res['Date'] = res['Date'].apply(lambda x: x.strftime('%m/%d/%Y'))
            res['Date'] = res['Date'].apply(str) + ' ' + res['Start_time']
            m_nos = df['Meter Number'].unique()
            for m_no in m_nos:
                data_obj = {}
                data_obj['source_file_location'] = file_location
                temp_df = res
                df = temp_df[temp_df['Meter Number'] == m_no]
                df = df[['Date', 'value']]
                df.columns = ['Timestamp', 'Original']
                data_obj['file_name'] = "sce_loa_output_"+m_no+ "_" + time_stamp + str(".xlsx")
                filename = str(output_location) + data_obj['file_name']
                self.etl_helper.writer_xls(df, filename)
                data_obj['file_location'] = output_location
                self.recordList.append(data_obj)
        else:
            data_obj = {}
            data_obj['error'] = "Invalid format"
            self.recordList.append(data_obj)
        return self.recordList
