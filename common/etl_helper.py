import numpy as np
import pandas as pd
from pandas import DataFrame
#import pandas.io.data
from pandas_datareader import data, wb
import xlsxwriter
from openpyxl.writer.excel import ExcelWriter
import datetime
from datetime import datetime

class EtlHelper:

    def read_xls(self,file_location):
        xls_file = pd.ExcelFile(file_location)
        xls_file.sheet_names
        return xls_file
    def read_csv(self,file_location,seperator=',',skiprows=0,col_arr=[]):
        if (skiprows>0 and len(col_arr)>0):
            df = pd.read_csv(file_location, sep=seperator, skiprows=skiprows,
                             usecols=col_arr)
        elif (skiprows>0):
            df = pd.read_csv(file_location, sep=seperator, skiprows=skiprows)
        elif (len(col_arr)>0):
            df = pd.read_csv(file_location, sep=seperator,usecols=col_arr)
        elif (skiprows == 0 and len(col_arr) == 0):
            df = pd.read_csv(file_location, sep=seperator)
        else:
            df = ''
        return df

    def remove_columns_xls(self,df,col_arr):
        my_cols = set(df.columns)
        for col in col_arr:
            my_cols.remove(col)
        my_cols = list(my_cols)
        df = df[my_cols]
        return df

    def melt(self,df,id_vars,var_name):
        res = pd.melt(df, id_vars=id_vars, var_name=var_name)
        return res

    def writer_xls(self,df,filename):
        writer = pd.ExcelWriter(filename, engine='xlsxwriter')
        df.to_excel(writer, sheet_name='Sheet1', index=False)

    def to_datetime(self,df,format):
        df = pd.to_datetime(df, format=format)
        return df

    def obj_dict(obj):
        return obj.__dict__
